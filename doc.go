package main

/*
* 1、保证gogen.exe放在GOBIN目录里面，比如C:/Go/bin
* 2、支持命令: build, install, run, test
 */

/* 自定义 gogen-build-system 模版, save by yourself name, 位置:Tools->Build System->New Build System
*  第一个参数为gogen.exe绝对路径，如果环境变量中已经定义GOBIN，gogen放哪都行
*  参数"$file_name", "$file_path"必须放到最后两个位置，中间的编译参数随便
{
    "cmd": ["gogen"],
    "file_regex": "^[ ]*File \"(...*?)\", line ([0-9]*)",
    "working_dir": "$file_path",
    "selector": "source.go",
    "variants":[
        { "name": "Build",
          "cmd": ["gogen", "build", "-i", "$file_name", "$file_path"]
        },

        { "name": "Run",
          "cmd": ["gogen", "run", "$file_name", "$file_path"]
        },

        { "name": "Install",
          "cmd": ["gogen", "install", "-v", "$file_name", "$file_path"]
        },

        { "name": "Clean",
          "cmd": ["gogen", "clean", "-x", "-i", "$file_name", "$file_path"]
        },

        { "name": "Test",
          "cmd": ["gogen", "test -v", "$file_name", "$file_path"]
        },
    ],
}
*/

/*Key Bindings User，自定义按键命令映射表，位置:Preferences->Key Bindings User
[
   { "keys": ["ctrl+shift+space"], "command": "build", "args": {"variant": "Build"}  },
   { "keys": ["ctrl+shift+enter"], "command": "build", "args": {"variant": "Install"}  },
   { "keys": ["ctrl+shift+\\"], "command": "build", "args": {"variant": "Run"}  },
   { "keys": ["ctrl+shift+delete"], "command": "build", "args": {"variant": "Clean"}  },
   { "keys": ["ctrl+shift+/"], "command": "build", "args": {"variant": "Test"}  },
   { "keys": ["command+shift+delete"], "command": "golang_build_cancel" },
]
*/
